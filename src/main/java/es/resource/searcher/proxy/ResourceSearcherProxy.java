package es.resource.searcher.proxy;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.type.TypeReference;

import es.resource.searcher.proxy.constants.ProxyConstants;
import es.valhalla.entities.ValhallaServiceRegistry;
import es.valhalla.utils.jsonserializer.JsonSerializeUtils;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.proxy.handler.ProxyHandler;
import io.vertx.httpproxy.HttpProxy;

public class ResourceSearcherProxy {

	private static Map<String, List<ValhallaServiceRegistry>> registry = new HashMap<>();

	private final static ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(ResourceSearcherProxy.class);

	private static String serviceRegistryPath;
	
	private static HttpClient proxyClient;
	private static Router proxyRouter;

	public static void main(String[] args) {

		serviceRegistryPath = System.getProperty(ProxyConstants.SERVICE_REGISTRY_PROP);

		if (serviceRegistryPath == null) {
			serviceRegistryPath = ProxyConstants.DEFAULT_REGISTRY_PATH;
		}
		LOGGER.info("Starting proxy deployment");
		final Vertx vertx = Vertx.vertx();
		retriever(vertx);
		final HttpServer proxyServer = vertx.createHttpServer();
		proxyRouter = Router.router(vertx);
		proxyRouter.route().handler(CorsHandler.create("*").allowedMethods(allowedMethods()).allowedHeaders(allowedHeaders()));
		proxyServer.requestHandler(proxyRouter);
		proxyServer.listen(8080);
		proxyClient = vertx.createHttpClient();
		populateRegisty();
	}

	private static void populateRegisty() {
		registry.forEach((k, v) -> {

			LOGGER.info("REGISTERING {} app", k);
			for (final ValhallaServiceRegistry service : v) {

				final HttpProxy httpProxy = HttpProxy.reverseProxy(proxyClient);
				httpProxy.origin(8080, service.getServicePath());
				proxyRouter.route(HttpMethod.valueOf(service.getMethod()), service.getServicePath())
						.handler(ProxyHandler.create(HttpProxy.reverseProxy(proxyClient), service.getPort(), service.getServiceAddress()));

			}
			LOGGER.info("REGISTERED {} app", k);
		});


	}

	private static void retriever(final Vertx vertx) {

		ConfigStoreOptions fileStore = new ConfigStoreOptions().setType("file")
				.setConfig(new JsonObject().put("path", serviceRegistryPath));

		ConfigRetrieverOptions options = new ConfigRetrieverOptions().setScanPeriod(2000).addStore(fileStore);

		ConfigRetriever retriever = ConfigRetriever.create(vertx, options);
		retriever.getConfig(json -> {
			if (json.succeeded()) {
				LOGGER.info("REGISTERING");
				registry = JsonSerializeUtils.deserialize(json.result().toString(),
						new TypeReference<Map<String, List<ValhallaServiceRegistry>>>() {
						});
				populateRegisty();
			}

		});

		retriever.listen(change -> {
			LOGGER.info("UPDATING REGISTRY");
			registry = JsonSerializeUtils.deserialize(change.getNewConfiguration().toString(),
					new TypeReference<Map<String, List<ValhallaServiceRegistry>>>() {
					});
			populateRegisty();
		});

	}
	
	private static Set<String> allowedHeaders() {
		final Set<String> allowedHeaders = new HashSet<>();
		allowedHeaders.add("x-requested-with");
		allowedHeaders.add("Access-Control-Allow-Origin");
		allowedHeaders.add("origin");
		allowedHeaders.add("Content-Type");
		allowedHeaders.add("accept");
		allowedHeaders.add("Access-Control-Allow-Credentials");
		allowedHeaders.add("Access-Control-Allow-Headers");
		allowedHeaders.add("origin");
		allowedHeaders.add("authorization");
		allowedHeaders.add("responseType");
		allowedHeaders.add("content-disposition");
		allowedHeaders.add("host");
		allowedHeaders.add("referer");
		return allowedHeaders;
	}
	
	private static Set<HttpMethod> allowedMethods() {
		final Set<HttpMethod> allowedMethods = new HashSet<>();
		allowedMethods.add(HttpMethod.GET);
		allowedMethods.add(HttpMethod.POST);
		allowedMethods.add(HttpMethod.DELETE);
		allowedMethods.add(HttpMethod.PATCH);
		allowedMethods.add(HttpMethod.OPTIONS);
		allowedMethods.add(HttpMethod.PUT);
		return allowedMethods;
	}
	
	

}
