docker container stop human-resources
docker container rm human-resources
docker image rm human-resources:1.0.0
docker build -t human-resources:1.0.0 .
docker run -it -d --name="human-resources" -v C:/dockerrrhh:/conf --network HUMAN_RESOURCES --ip 172.21.0.10 -p 8080:8080 human-resources:1.0.0